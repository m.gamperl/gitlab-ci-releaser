'use strict';

const childProcess = require('child-process-promise');

function run(command) {
  return childProcess.exec(command)
    .then(result => result.stdout);
}

module.exports = {
  run,
};
