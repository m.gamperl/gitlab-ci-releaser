'use strict';

const { run: runCommand } = require('../utils/commands');

const {
  CI_REPOSITORY_URL,
  CI_JOB_ID,
  CI_COMMIT_REF_NAME,
  GITLAB_CI_RELEASER_NAME,
  GITLAB_CI_RELEASER_EMAIL,
  GITLAB_CI_RELEASER_TOKEN,
  GITLAB_USER_EMAIL,
} = process.env;

function commitFile(message, file, username, usermail) {
  const gitConfig = `-c user.email="${usermail}" -c user.name="${username}"`;
  return runCommand(`git ${gitConfig} commit -m ${message} -- ${file}`)
    .then(() => runCommand('git log -n 1 --pretty="%H"'))
    .then(hash => hash.trim());
}

function push(username, token) {
  const remoteUrl = CI_REPOSITORY_URL.replace(/gitlab-ci-token[^@]*/, `${username}:${token}`);
  const remoteName = `gitlabcireleaser-${CI_JOB_ID}`;
  const branchName = `HEAD:${CI_COMMIT_REF_NAME}`;
  return runCommand(`git remote add ${remoteName} ${remoteUrl}`)
    .then(() => runCommand(`git push ${remoteName} ${branchName}`))
    .then(() => runCommand(`git remote rm ${remoteName}`));
}

function commitAndPush(message, file, token) {
  const username = GITLAB_CI_RELEASER_NAME || '';
  const usermail = GITLAB_CI_RELEASER_EMAIL || GITLAB_USER_EMAIL;
  const authenticationToken = token || GITLAB_CI_RELEASER_TOKEN;
  return commitFile(message, file, username, usermail)
    .then(hash => push(username, authenticationToken).then(() => hash));
}

module.exports = commitAndPush;
