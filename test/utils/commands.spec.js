/* eslint-disable no-param-reassign */
'use strict';

const test = require('ava');
const sinon = require('sinon');
const childProcess = require('child-process-promise');
require('../helpers/mockPackageJson');

test.beforeEach(() => {
  sinon.stub(childProcess, 'exec', command => Promise.resolve({
    stdout: `${command} executed!`,
  }));
});

test.afterEach(() => {
  childProcess.exec.restore();
});

test('must execute "npm publish"', (t) => {
  const commands = require('../../src/utils/commands');
  return commands.run('supercommand -p1=param1 --param2')
    .then((output) => {
      t.true(childProcess.exec.calledOnce);
      t.true(childProcess.exec.calledWithExactly('supercommand -p1=param1 --param2'));
      t.is(output, 'supercommand -p1=param1 --param2 executed!');
    });
});
